import argparse
import time
import nltk
import jinja2
from empath import Empath
from textblob import TextBlob
from nltk.corpus import opinion_lexicon
from nltk.sentiment import SentimentIntensityAnalyzer
from langdetect import detect, DetectorFactory
from langdetect.lang_detect_exception import LangDetectException

DetectorFactory.seed = 0
lexicon = Empath()

def detect_language(text):
    try:
        language = detect(text)
        return language
    except LangDetectException as e:
        return f"Error: {str(e)}"

def setup_nltk():
    nltk.download('punkt')
    nltk.download('opinion_lexicon')
    nltk.download('vader_lexicon')

def sentiment_analysis(text):
    blob = TextBlob(text)
    return blob.sentiment

def vader_sentiment_analysis(text):
    sia = SentimentIntensityAnalyzer()
    return sia.polarity_scores(text)

def count_words_by_sentiment(text):
    positive_words = set(opinion_lexicon.positive())
    negative_words = set(opinion_lexicon.negative())

    words = nltk.word_tokenize(text.lower())

    positive_count = sum(1 for word in words if word in positive_words)
    negative_count = sum(1 for word in words if word in negative_words)

    return negative_count, positive_count


def read_text_file(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            contents = file.read()
            return contents
    except FileNotFoundError:
        print(f"Error: The file at '{file_path}' was not found.")
    except PermissionError:
        print("Error: Permission denied to read the file at '{file_path}'.")
    except Exception as e:
        print(f"An unexpected error occurred: {str(e)}")

def clean_skipped_categories(empath_category_analysis, categories_to_skip):
    cleaned_result = empath_category_analysis
    for category in categories_to_skip.split(","):
        try:
            if category.strip():
                del cleaned_result[category.strip()]
        except KeyError:
            print(f"The category {category} was not found in the analysis...")
    return cleaned_result

def analyse_text_categories(text):
    category_analysis = lexicon.analyze(text, normalize=True)
    filter_zero_values = lambda obj: {key: value for key, value in obj.items() if value > 0.0}
    return filter_zero_values(category_analysis)

def calculate_vader_sentiment(compound):
    if compound >= 0.05:
        return "positive"
    if compound <= 0.05:
        return "negative"
    return "neutral"

def generate_html(text, json_data):
    template_loader = jinja2.FileSystemLoader(searchpath="./")
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template("template.html")

    rendered_html = template.render(data=json_data, text=text)

    report_file_name = f"reports/sentiment_analysis_{time.time()}.html"
    with open(report_file_name, 'w', encoding='utf-8') as file:
        file.write(rendered_html)
    print("-"*100)
    print(f"[+] Report saved as {report_file_name}!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Analyze and generate a report for the given text about language, sentiment, behavior, and topics.")
    parser.add_argument("-s", "--source", type=str, help="Text file path to be analysed", required=True)
    parser.add_argument("--skip-categories", type=str, help="Comma separated categories to skip", default="")
    args = parser.parse_args()

    setup_nltk()

    file_contents = read_text_file(args.source)
    
    text_categories = analyse_text_categories(file_contents)
    text_categories = clean_skipped_categories(text_categories, args.skip_categories)
    sentiment = sentiment_analysis(file_contents)
    negative_words_count, positive_words_count = count_words_by_sentiment(file_contents)
    vader_sentiment = vader_sentiment_analysis(file_contents)
    result = {
        "language": detect_language(file_contents),
        "categories": text_categories,
        "sentiment": {
            "polarity": sentiment.polarity,
            "subjectivity": sentiment.subjectivity,
            "negative_words_count": negative_words_count,
            "positive_words_count": positive_words_count,
            "vader": {
                "compound": vader_sentiment.get("compound"),
                "sentiment": calculate_vader_sentiment(vader_sentiment.get("compound")),
                "negative": vader_sentiment.get("neg"),
                "neutral": vader_sentiment.get("neu"),
                "positive": vader_sentiment.get("pos")
            }
        }
    }
    generate_html(file_contents, result)
    #print(json.dumps(result, sort_keys=True, indent=4))
# sentiment-text-analyzer

> This project is a PoC

## Description

This tool aims to analyse and generate a report about the sentiment of a given text using Natural Language Processing (NLP) with various Python libraries such as NLTK, TextBlob, and VADER to perform comprehensive sentiment analysis.

![](assets/report1.png)
![](assets/report2.png)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE.md) file for details.

## NLP Frameworks

* Empath - (https://github.com/Ejhfast/empath-client)[https://github.com/Ejhfast/empath-client]
* LNTK - (https://www.nltk.org/)[https://www.nltk.org/]
* TextBlob - (https://textblob.readthedocs.io/en/dev/)[https://textblob.readthedocs.io/en/dev/]

## Features

Report generation with:

* **Word Categories Mapping:** Categorises words based on their contextual usage.
* **Sentiment Analysis:** Provides detailed sentiment metrics including polarity, subjectivity, and VADER sentiment scores.
* **VADER Analysis:** Utilises the VADER sentiment analysis tool to quantify the positive, neutral, and negative sentiment in the text.

## Natural Language Processing (NLP) usage in the Script

The script uses several NLP libraries to analyse the text:

1. NLTK (Natural Language Toolkit):

* Tokenizes the text into words and sentences.
* Performs part-of-speech tagging and text classification.
* Helps in identifying and counting positive and negative words in the text.

2. TextBlob:

* Computes the polarity and subjectivity of the text.
* Polarity ranges from -1 (negative) to 1 (positive).
* Subjectivity ranges from 0 (objective) to 1 (subjective).

3. VADER (Valence Aware Dictionary and Sentiment Reasoner):

> Hutto, C.J. & Gilbert, E.E. (2014). VADER: A Parsimonious Rule-based Model for Sentiment Analysis of Social Media Text. Eighth International Conference on Weblogs and Social Media (ICWSM-14). Ann Arbor, MI, June 2014.

* Specifically tuned for analysing social media text.
* Provides a compound score that indicates the overall sentiment of the text.
* Also provides individual scores for positive, neutral, and negative sentiments.

## Setup
![](https://img.shields.io/badge/Python->3.9.15-green)

Install a Python version higher than 3.9.15: [https://wiki.python.org/moin/BeginnersGuide/Download](https://wiki.python.org/moin/BeginnersGuide/Download)

Using the Terminal available for your system:

````shell
git clone <project_url>
cd <project_url>
pip install -r requirements.txt
````

## Usage

Using the Terminal available for your system:

* Help:

````shell
$ python sentiment-text-analyzer.py -h

usage: sentiment-text-analizer.py [-h] -s SOURCE [--skip-categories SKIP_CATEGORIES]

Analyse and generate a report for the given text about language, sentiment, behavior, and topics.

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        Text file path to be analyzed
  --skip-categories SKIP_CATEGORIES
                        Comma separated categories to skip

````

* Text Analysis

````shell
$ python python lnp-text-analyzer.py --source ~/example.txt

[nltk_data] Downloading package punkt to /Users/test/nltk_data...
[nltk_data]   Package punkt is already up-to-date!
[nltk_data] Downloading package opinion_lexicon to
[nltk_data]     /Users/test/nltk_data...
[nltk_data]   Package opinion_lexicon is already up-to-date!
[nltk_data] Downloading package vader_lexicon to
[nltk_data]     /Users/test/nltk_data...
[nltk_data]   Package vader_lexicon is already up-to-date!
----------------------------------------------------------------------------------------------------
[+] Report saved as sentiment_analysis_1715910432.3917272.html!

````

* Text Analysis (skipping categories)

````shell
$ python python lnp-text-analyzer.py --source ~/example.txt --skip-categories night,sleep

[nltk_data] Downloading package punkt to /Users/test/nltk_data...
[nltk_data]   Package punkt is already up-to-date!
[nltk_data] Downloading package opinion_lexicon to
[nltk_data]     /Users/test/nltk_data...
[nltk_data]   Package opinion_lexicon is already up-to-date!
[nltk_data] Downloading package vader_lexicon to
[nltk_data]     /Users/test/nltk_data...
[nltk_data]   Package vader_lexicon is already up-to-date!
----------------------------------------------------------------------------------------------------
[+] Report saved as sentiment_analysis_1715910432.3917272.html!

````

After running those commands an output_<unix_time>.html report file should be generated in the reports folder of the project root directory.

## Credits
* Manuel Maria Camero Orellana (Developer)
* Maria Camero Orellana (Psychologist)
* Victorule (Logo)[https://www.flaticon.es/iconos-gratis/sentimientos]
